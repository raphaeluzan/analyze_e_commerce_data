import os
import boto3

bucket_name = os.environ["BUCKET_NAME"]
aws_access_key_id = os.environ["AWS_ACCESS_KEY_ID"]
aws_secret_access_key = os.environ["AWS_SECRET_ACCESS_KEY"]

# enter authentication credentials
s3 = boto3.resource(
    "s3",
    aws_access_key_id=aws_access_key_id,
    aws_secret_access_key=aws_secret_access_key,
)

for KEY in ["customer.csv", "items.csv", "products.csv", "orders.csv"]:
    s3.Bucket(bucket_name).download_file(KEY, KEY)
