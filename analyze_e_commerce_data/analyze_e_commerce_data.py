import sys
import pyspark
from pyspark.sql import functions as F
from pyspark.sql import SparkSession
from pyspark.sql.functions import col
from pyspark.sql.types import DateType, IntegerType

# create the session
conf = pyspark.SparkConf().set("spark.ui.port", "4050")

# create the context
sc = pyspark.SparkContext(conf=conf)
spark = SparkSession.builder.getOrCreate()

# q1 Get the four datasets into Spark
df_customer = spark.read.option("header", True).csv("customer.csv")
df_item = spark.read.option("header", True).csv("items.csv")
df_product = spark.read.option("header", True).csv("products.csv")
df_order = spark.read.option("header", True).csv("orders.csv")

df_join_order = df_customer.join(df_order, ["customer_id"])
df_join_items = df_product.join(df_item, ["product_id"])
df_join = df_join_order.join(df_join_items, ["order_id"])

df_join_with_date = df_join.withColumn(
    "record_date", df_join["order_approved_at"].cast(DateType())
)
df_join_with_date = df_join_with_date.withColumn(
    "price_as_int", df_join_with_date["price"].cast(IntegerType())
)


if len(sys.argv) == 2:
    # q2 compute for a given day these summary statistics
    date = sys.argv[1]
    df = df_join_with_date.filter(df_join_with_date.record_date == date)
if len(sys.argv) == 3:
    # q3 Run that script over the necessary period
    date_debut = sys.argv[1]
    date_fin = sys.argv[2]
    df = df_join_with_date.filter(df_join_with_date.record_date >= date_debut)
    df = df.filter(df_join_with_date.record_date < date_fin)
else:
    raise ValueError(
        "You can put 1 or 2 dates as parameters with this format : DD-MM-YYYY"
    )

# q4 identify the top customers
df = df.groupBy("customer_unique_id").agg(
    F.sum("price_as_int"), F.count("price_as_int")
)
df_top_customer = df.sort(col("sum(price_as_int)").desc()).show(
    20, truncate=False
)
df_top_customer.write.csv("top_customer.csv")

# q5 How many customers are repeaters ?
df_repeater = df.filter(df["count(price_as_int)"] > 1).sort(
    col("count(price_as_int)").desc()
)
df_repeater.show(20, truncate=False)
repeaters_count = df_repeater.count()
print(f"There is {str(repeaters_count)} repeaters")
df_repeater.write.csv("repeaters.csv")
