# analyze_e_commerce_data

## projet Target et projet current 

### Target : 
![schema de la target](images/target.png)

* Le DevOps lance le terraform
* Le terraform build le cluster EMR
* Le terraform build le cluster Elastic/kibana
* Le terraform lance le data pipeline avec le script recuperé sur gitlab et un scheduling
* Le script pyspark lit depuis S3 et pousse le resultat des aggrégations sur Elastic

### Current : 
![schema de l'etat current](images/current.png)
* Le DevOps lance le terraform
* Le terraform build le cluster Elastic/kibana
* le devOps recupere le code et le met sur S3
* Le devOps crée un cluster EMR puis ajoute un step avec le script pyspark qui se trouve sur S3
* Le script pyspark lit depuis S3 et pousse le resultat des aggrégations en CSV localement


## Etapes de travail 

### 1/ Découverte des données (Voir Notebook)

* liens entre les tables :white_check_mark:
* Ajout des CSV dans S3: white_check_mark:
* Analyse de qualité des données :white_check_mark:


### 2/ Script pyspark

* Charger spark dans google collab :white_check_mark:
* Charger les données dans Google collab depuis S3 :white_check_mark:
* Faire les jointures :white_check_mark:
* transformer la date en jour de type date :red_circle:
* faire les bons filter/groupBy pour repondre aux questions :white_check_mark:

1. Exemple d'output : Repeaters
![repeaters](images/repeaters.png)

    * 83.551 clients n'ont passé qu'une seul commande

    * 11870 clients on passé au moins 2 commandes

2. Exemple d'output : top customers
![top_customers](images/top_customer.png)
    * Nous pouvons voir ici les plus gros clients ainsi que le nombre total de commande sur la période

### 3/ Passer du notebook a un script parametrable
* Permettre de lancer le script pour un jour donné au format  :white_check_mark:
> ./script.py 20-02-2021
* Permettre de lancer le script pour un range de jours au format :white_check_mark:
> ./script.py 20-02-2021 30-02-2021

### 4/ gitlab CI
* Linter :white_check_mark:
* Lancer les tests pysparks :white_check_mark:
* Créer une release uniquement si on est sur la branch main :white_check_mark:

### 5/ tests pyspark
* Ecrire des tests pyspark :white_check_mark:
* les lancer dans la CI :white_check_mark:
* Faire un niveau de coverage satisfaisant :red_circle:

### 6/ infra-as-code terraform avec elastic
* script terraform pour builder le cluster elastic :white_check_mark:
* Ajouter les options de securité pour ajouter mon ip directement dans le terraform :white_check_mark:

### 7/ infra-as-code terraform avec EMR
* Runer un script pyspark de manière manuel :white_check_mark:
* Terraform pour le build EMR : :red_circle: 

### 8/ infra-as-code terraform aws data pipeline
* Plannifier l'execution d'un script : :red_circle:
